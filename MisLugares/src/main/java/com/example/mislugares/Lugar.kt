package com.example.mislugares

class Lugar(
    nombre: String,
    direccion: String,
    latitud: Double,
    longitud: Double,
    foto: String,
    tipo: TipoLugar,
    telefono: Int,
    url: String,
    comentario: String,
    valoracion: Int
) {
    var nombre: String
    var direccion: String
    var posicion: GeoPunto
    var tipo: TipoLugar
    var foto: String
    var telefono: Int
    var url: String
    var comentario: String
    var fecha: Long
    var valoracion: Float

    //constructor alternativo recibiendo GeoPunto en lugar de latitud y longitud
    constructor(
        nombre: String,
        direccion: String,
        p: GeoPunto,
        foto: String,
        tipo: TipoLugar,
        telefono: Int,
        url: String,
        comentario: String,
        valoracion: Int
    ) : this(
        nombre, direccion, p.latitud, p.longitud, foto,
        tipo, telefono, url, comentario, valoracion
    ) {
    }

    override fun toString(): String {
        return "Lugar{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", posicion=" + posicion +
                ", tipo=" + tipo +
                ", foto='" + foto + '\'' +
                ", telefono=" + telefono +
                ", url='" + url + '\'' +
                ", comentario='" + comentario + '\'' +
                ", fecha=" + fecha +
                ", valoracion=" + valoracion +
                '}'
    }

    init {
        fecha = System.currentTimeMillis()
        posicion = GeoPunto(latitud, longitud)
        this.foto = foto
        this.tipo = tipo
        this.nombre = nombre
        this.direccion = direccion
        this.telefono = telefono
        this.url = url
        this.comentario = comentario
        this.valoracion = valoracion.toFloat()
    }
}